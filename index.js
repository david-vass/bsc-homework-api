const express = require('express');
const cors = require('cors');

const notesRouter = require('./notes');

const app = express();
const PORT = 3001;

app.use(express.json())
app.use(cors({
  origin: 'http://localhost:3000',
}));
app.get('/', (req, res) => res.send('Hello!'));
app.use('/notes', notesRouter);

app.listen(PORT, () => console.log(`Example app listening on port ${PORT}!`));
