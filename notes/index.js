const express = require('express');

const config = require('../config');

const Notes = require('./notesModel');


const timeout = (ms) => (
  new Promise((resolve) => setTimeout(resolve, ms))
);

const notes = express.Router();

// List
notes.get('/', async (req, res) => {
  try {
    const list = await Notes.getList();
    
    res.json(list);
  } catch (e) {
    res.status(500).json({ error: 'Unknown error', message: e.message });
  }
});

// Create
notes.post('/', async (req, res) => {
  if (!req.body) {
    return res.status(422).json({ error: 'Missing input!' });
  }
  const {title, body} = req.body;
  try {
    const newId = await Notes.getNewID();
    const note = {
      id: newId,
      title,
      body,
      lastUpdated: new Date().toISOString(),
      createdAt: new Date().toISOString(),
    };
    await Notes.write(newId, note);
    res.json({ id: newId, title, body });
  } catch (e) {
    res.status(500).json({ error: e.message })
  }
});

// Update
notes.put('/:id', async (req, res) => {
  if (!req.body) {
    return res.status(422).json({ error: 'Missing input!' });
  }
  const {
    title,
    body,
  } = req.body;
  const {id} = req.params;

  try {
    await Notes.write(id, {
      id,
      body,
      title,
    });
    res.json({ ok: 'ok' });
  } catch (e) {
    res.status(500).json({ error: e.message })
  }
});

// Load
notes.get('/:id', async (req, res) => {
  const {id} = req.params;

  try {
    const note = await Notes.read(id);
    res.json(note);
  } catch (e) {
    console.log('e', e.message);
    res.status(404).json({ error: 'Note does not exist!' });
  }
});

// Delete
notes.delete('/:id', async (req, res) => {
  const {id} = req.params;

  try {
    await Notes.remove(id);
  } catch (e) {}

  res.json({ ok: 'ok' });
});


module.exports = notes;
