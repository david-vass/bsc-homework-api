const path = require('path');

const ROOT_DIR = path.resolve(__dirname);


module.exports = {
  rootDir: ROOT_DIR,
  dbDir: path.join(ROOT_DIR, 'db'),
};
