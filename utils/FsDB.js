const fs = require('fs');
const path = require('path')
const {v4: uuid} = require('uuid');

const config = require('../config');


class FsDB {
  constructor(dirname) {
    this.dirname = dirname;

    this.fullPath = path.join(config.dbDir, dirname);
    if (!fs.existsSync(this.fullPath)) {
      fs.mkdirSync(this.fullPath, {recursive: true});
    };
  }

  write(filename, content) {
    console.log("path", `${this.fullPath}${filename}.json`);
    return fs.promises.writeFile(
      `${this.fullPath}/${filename}.json`,
      JSON.stringify(content, undefined, 2),
    );
  }

  read(filename) {
    return fs.promises.readFile(
      `${this.fullPath}/${filename}.json`
    )
      .then(JSON.parse);
  }

  remove(filename) {
    return fs.promises.unlink(
    `${this.fullPath}/${filename}.json`
    )
  }

  getList() {
    return fs.promises.readdir(this.fullPath)
      .then((content) => Promise.all(
        content.map((filename) => (
          this.read(filename.replace('.json', ''))
        ))
      ));
  }

  getNewID() {
    return this.getList()
      .then((existingIDs) => {
        let newId;
        do {
          newId = uuid();
        } while (existingIDs.includes(newId))

        return newId;
      });
  }
}


module.exports = FsDB;
