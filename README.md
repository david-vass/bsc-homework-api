# David Vass - BSC homework API

This is simple API that I've been using for my prototypes. It uses
file system instead of database and all records are stored as jsons.
So it's dead simple to check anything that has been saved here. It's
bare bones, no validations, no nothing except saving stuff to file system.
Definitely not production ready in any case.

## Getting started

1. Create `db` directory, where all records will be saved
2. Run `npm start`

Note:
`db` dir will be created on it's own, but nodemon will start restarting
because apparently ignore rule works only for existing dirs, not
dirs created during run.

